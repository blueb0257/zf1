<?php

class Admin_Bootstrap extends Zend_Application_Module_Bootstrap {

    public function _initAdminAutoloaderConfig() {
        $config = array(
            'Zend_Loader_ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
                APPLICATION_PATH . '/../library/autoload_classmap.php',
            ),
            'Zend_Loader_StandardAutoloader' => array(
                'prefixes' => array(
                    'Zend' => APPLICATION_PATH . '/../library/Zend'
                ),
                'fallback_autoloader' => false
            ),
        );
        Zend_Loader_AutoloaderFactory::factory($config);
    }

}
