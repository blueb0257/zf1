<?php

class Admin_MenuitemController extends Zend_Controller_Action {

    protected $_menuitemTable;
    protected $_menuTable;

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {
        $menuId = (int) $this->_request->getParam('menuid');
        if ($menuId == 0) {
            return;
        }

        $this->view->menu = $this->getMenuTable()->get($menuId);
        $this->view->items = $this->getMenuitemTable()->getItemsByMenuId($menuId);
    }

    public function addAction() {
        // Get request uri
        $request = $this->getRequest();
        $uri = $request->getRequestUri();

        $menuId = (int) $request->getParam('menuid');

        $this->view->menu = $this->getMenuTable()->get($menuId);
        $frmMenuItem = new Admin_Form_MenuItemForm();
        if ($request->isPost()) {
            if ($frmMenuItem->isValid($_POST)) {
                $data = $frmMenuItem->getValues();
                $data['menu_id'] = $menuId;
                $this->getMenuitemTable()->save($data);

                return $this->_helper->redirector('index', $request->getControllerName(), $request->getModuleName(), array('menuid' => $data['menu_id']));
            }
        }
        $frmMenuItem->populate(array('menu_id' => $menuId));
        $this->view->form = $frmMenuItem;
    }

    public function moveAction() {
        // Get request uri
        $request = $this->getRequest();
        $uri = $request->getRequestUri();
        
        $id = (int) $request->getParam('id');
        $direction = $request->getParam('direction');

        if ($direction == 'up') {
            $this->getMenuitemTable()->moveUp($id);
        } elseif ($direction == 'down') {
            $this->getMenuitemTable()->moveDown($id);
        }
        
        $menuItem = $this->getMenuitemTable()->get($id);
        
        return $this->_helper->redirector('index', $request->getControllerName(), $request->getModuleName(), array('menuid' => $menuItem['menu_id']));
    }

    public function updateAction() {
        // Get request uri
        $request = $this->getRequest();
        $uri = $request->getRequestUri();

        $id = (int) $this->_request->getParam('id');

        // fetch the current item 
        $currentMenuItem = $this->getMenuitemTable()->get($id);

        // create and populate the form instance 
        $frmMenuItem = new Admin_Form_MenuItemForm();
        $frmMenuItem->setAction($uri);

        // process the postback 
        if ($request->isPost()) {
            if ($frmMenuItem->isValid($_POST)) {
                $data = $frmMenuItem->getValues();
                $this->getMenuitemTable()->save($data);
//                $request->setParam('menuid', $data['menu_id']);
                return $this->_helper->redirector('index', $request->getControllerName(), $request->getModuleName(), array('menuid' => $data['menu_id']));
            }
        } else {
            $frmMenuItem->populate($currentMenuItem);
        }
        // fetch its menu 
        $this->view->menu = $this->getMenuTable()->get($currentMenuItem['menu_id']);

        $this->view->form = $frmMenuItem;
    }

    public function deleteAction() {
        // Get request uri
        $request = $this->getRequest();
        $uri = $request->getRequestUri();

        $id = (int) $request->getParam('id');

        $currentMenuItem = $this->getMenuitemTable()->get($id);

        $this->getMenuitemTable()->deleteById($id);
        
        return $this->_helper->redirector('index', $request->getControllerName(), $request->getModuleName(), array('menuid' => $currentMenuItem['menu_id']));
    }

    function getMenuitemTable() {
        if (!$this->_menuitemTable) {
            $model = new Admin_Model_Menuitem();
            $this->_menuitemTable = $model;
        }

        return $this->_menuitemTable;
    }

    function getMenuTable() {
        if (!$this->_menuTable) {
            $model = new Admin_Model_Menu();
            $this->_menuTable = $model;
        }

        return $this->_menuTable;
    }

}
