<?php

class Admin_GaController extends Zend_Controller_Action {

    protected $_ga;
    protected $_gaHelpers;

    public function init() {
        try {
            Zend_Session::start();
        } catch (Zend_Session_Exception $e) {
            session_start();
        }
        /** Initialize action controller here */
        $this->_helper->layout->setLayout('/admin/default/index');
    }

    public function indexAction() {
//        session_start();

        if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['token']) && $_POST['token'] == 'delete') {
            unset($_SESSION['token']);
        }

        $gaHelpers = new Admin_Model_GAHelpers(new Admin_Model_GA());
        $gaHelpers->init();
        $ga = $gaHelpers->getGa();
        $client = $ga->getClient();
//        $service = $ga->getService();

        $url = $this->getUrl();

        $client->setRedirectUri($url);

        if (isset($_GET['code'])) {
            $client->authenticate();
            $_SESSION['token'] = $client->getAccessToken();
            $this->redirect('admin/ga');
        }

        if (isset($_SESSION['token'])) {
            $client->setAccessToken($_SESSION['token']);
        }

        if (!$client->getAccessToken()) {
            $authUrl = $client->createAuthUrl();
            return $this->view->authUrl = $authUrl;
        }
    }

    public function digitalAction() {
        
    }

    public function mbndAction() {
        
    }

    public function marryAction() {
        
    }
    
    public function marrybabyAction() {
        
    }

    public function getServiceMbndAction() {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $gaHelpers = new Admin_Model_GAHelpers(new Admin_Model_GA());
            $serviceMbnd = $gaHelpers->getServiceMbnd();
            $data = array(
                'yesterday' => $this->view->escape($serviceMbnd['Yesterday']),
                'lastmonth' => $this->view->escape($serviceMbnd['LastMonth'])
            );

            return $this->_helper->json($data);
        }
    }

    public function getInfoGartAction($name = null) {
        if ($this->getRequest()->isXmlHttpRequest()) {
//            session_start();

            if ($name == null) {
                $name = $this->getRequest()->getParam('name');
            }

            // Get ga helper
            $gaHelpers = new Admin_Model_GAHelpers(new Admin_Model_GA());

            // Init client and service
            $gaHelpers->init();
            $ga = $gaHelpers->getGa();
            $client = $ga->getClient();
            $service = $ga->getService();

            if (isset($_SESSION['token'])) {
                $client->setAccessToken($_SESSION['token']);
            }

            if ($client->getAccessToken()) {
                $results = $gaHelpers->getGAOption($name, $service);
                $_SESSION['token'] = $client->getAccessToken();

                // Get GA info
                $info = $gaHelpers->getRealTimeRepost($results);

                $data = array(
//                    'profile' => $this->view->escape($info['profile']),
                    'total' => $this->view->escape($info['total']),
                );

                $this->_helper->json($data);
            } else {
                $authUrl = $client->createAuthUrl();
                $authUrl = $this->view->escape($authUrl);
                $this->_helper->json($authUrl);
            }
        }
    }

    private function getUrl() {
        $uri = $this->getRequest()->getParams();
        $module = $uri['module'];
        $controller = $uri['controller'];
        $action = $uri['action'];
        $base = 'http';
        if ($this->_request->getServer('HTTPS', 'off') !== 'off') {
            $base .= 's';
        }

        $host = $this->_request->getServer('SERVER_NAME', 'localhost');

        $url = $base . "://" . $host . "/" . $module . "/" . $controller;

        return $url;
    }

}
