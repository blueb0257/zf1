<?php

class Admin_UserController extends Zend_Controller_Action {

    protected $_userTable;
    
    public function init() {
//        $this->_helper->layout->setLayout('/admin/default/index');
    }

    public function indexAction() {
        // Get request uri
        $request = $this->_request;
        $uri = $request->getRequestUri();

        $auth = Zend_Auth::getInstance();

        if ($auth->hasIdentity()) {
            $this->view->identify = $auth->getIdentity();
        }
    }

    public function listAction() {
        $list = $this->getUserTable()->get();

        $this->view->users = $list;
    }

    public function createAction() {
        // Get request uri
        $request = $this->_request;
        $uri = $request->getRequestUri();

        // Get user form
        $frm = new Admin_Form_UserForm();
        $frm->setAction($uri);

        if ($request->isPost()) {
            if ($frm->isValid($_POST)) {
                $data = $frm->getValues();

                $this->getUserTable()->save($data);

                return $this->_helper->redirector->gotoSimple('list', $request->getControllerName(), $request->getModuleName(), array());
            }
        }

        $this->view->form = $frm;
    }

    public function updateAction() {
        // Get request uri
        $request = $this->_request;
        $uri = $request->getRequestUri();

        // Get user form
        $frm = new Admin_Form_UserForm();
        $frm->setAction($uri);
        $frm->removeElement('password');

        if ($request->isPost()) {
            if ($frm->isValid($_POST)) {
                // Get data from form
                $data = $frm->getValues();

                // Update
                $this->getUserTable()->save($data);

                // redirect to listAction
                return $this->_helper->redirector->gotoSimple('list', $request->getControllerName(), $request->getModuleName(), array());
            }
        }

        // Get id from params
        $id = (int) $request->getParam('id');

        // Get row by id
        $row = $this->getUserTable()->get($id);

        // populate to frm
        $frm->populate($row);

        $this->view->form = $frm;
    }

    public function passwordAction() {
        // Get request uri
        $request = $this->_request;
        $uri = $request->getRequestUri();

        // Get form with only password element
        $frm = new Admin_Form_UserForm();
        $frm->setAction($uri);
        $frm->removeElement('username');
        $frm->removeElement('first_name');
        $frm->removeElement('last_name');
        $frm->removeElement('role');

        if ($request->isPost()) {
            if ($frm->isValid($_POST)) {
                $data = $frm->getValues();

                $this->getUserTable()->updatePassword((int) $data['id'], $data['password']);

                return $this->_helper->redirector->gotoSimple('list', $request->getControllerName(), $request->getModuleName(), array());
            }
        }
        // Get id from params
        $id = (int) $request->getParam('id');

        // Get row by id
        $row = $this->getUserTable()->get($id);

        if ($row) {
            $frm->populate($row);
        } else {
            throw new Zend_Exception("User not found!");
        }

        $this->view->form = $frm;
    }

    public function deleteAction() {
        // Get request uri
        $request = $this->_request;
        $uri = $request->getRequestUri();

        // Get id from params
        $id = (int) $request->getParam('id');

        $this->getUserTable()->deleteById($id);

        return $this->_helper->redirector->gotoSimple('list', $request->getControllerName(), $request->getModuleName(), array());
    }

    public function loginAction() {
        // Get request uri
        $request = $this->_request;
        $uri = $request->getRequestUri();

        // Get user form
        $frm = new Admin_Form_UserForm();
        $frm->setAction($uri);
        $frm->removeElement('first_name');
        $frm->removeElement('last_name');
        $frm->removeElement('role');

        if ($request->isPost() && $frm->isValid($_POST)) {
            $data = $frm->getValues();

            // Setup auth adapter,
            $db = $this->getUserTable()->getAdapter();
            //create the auth adapter 
            $authAdapter = new Zend_Auth_Adapter_DbTable($db, 'users', 'username', 'password');
            //set the username and password 
            $authAdapter->setIdentity($data['username']);
            $authAdapter->setCredential(md5($data['password']));
            //authenticate 
            $result = $authAdapter->authenticate();
            if ($result->isValid()) {
                // store the username, first and last names of the user 
                $auth = Zend_Auth::getInstance();
                $storage = $auth->getStorage();
                $storage->write($authAdapter->getResultRowObject(array(
                            'username', 'first_name', 'last_name', 'role'
                )));
                return $this->_helper->redirector->gotoSimple('index', $request->getControllerName(), $request->getModuleName(), array());
            } else {
                $this->view->loginMessage = "Sorry, your username or password was incorrect";
            }
        }
        $this->view->form = $frm;
    }

    public function logoutAction() {
        $authAdapter = Zend_Auth::getInstance();
        $authAdapter->clearIdentity();
    }

    function getUserTable() {
        if (!$this->_userTable) {
            $model = new Admin_Model_User();
            $this->_userTable = $model;
        }

        return $this->_userTable;
    }

}
