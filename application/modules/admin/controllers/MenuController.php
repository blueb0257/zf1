<?php

class Admin_MenuController extends Zend_Controller_Action {

    protected $_menuTable;
    protected $_menuitemTable;

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {
        // action body 
    }

    public function listAction() {
        try {
            $list = $this->getMenuTable()->get();
        } catch (Exception $ex) {
            return $this->_helper->redirector('index', $request->getControllerName());
        }

        $this->view->menus = $list;
    }

    public function createAction() {
        // Get request uri
        $request = $this->getRequest();
        $uri = $request->getRequestUri();

        // Get menu form
        $frm = new Admin_Form_MenuForm();
        $frm->setAction($uri);

        if ($request->isPost()) {
            if ($frm->isValid($_POST)) {
                $data = $frm->getValues();

                try {
                    $this->getMenuTable()->save($data);
                } catch (Exception $ex) {
                    return $this->_helper->redirector('create', $request->getControllerName());
                }

                return $this->_helper->redirector('list', $request->getControllerName());
            }
        }

        $this->view->form = $frm;
    }

    public function editAction() {
        // Get request uri
        $request = $this->getRequest();
        $uri = $request->getRequestUri();

        // Get id from params
        $id = (int) $request->getParam('id', 0);
        // If not exist
        if (!$id) {
            return $this->_helper->redirector('list', $request->getControllerName());
        }

        // Try to find row by id
        try {
            $row = $this->getMenuTable()->get($id);
        } catch (Exception $ex) {
            return $this->_helper->redirector('list', $request->getControllerName());
        }

        // Get menu form
        $frm = new Admin_Form_MenuForm();
        $frm->setAction($uri);

        // If post
        if ($request->isPost()) {
            if ($frm->isValid($_POST)) {
                $data = $frm->getValues();

                // Try to update
                try {
                    $this->getMenuTable()->save($data);
                } catch (Exception $ex) {
                    return $this->_helper->redirector('list', $request->getControllerName());
                }

                return $this->_helper->redirector('list', $request->getControllerName());
            }
        }

        $frm->populate($row);

        $this->view->form = $frm;
    }

    public function deleteAction() {
        // Get request uri
        $request = $this->getRequest();
        $uri = $request->getRequestUri();

        // Get id from params
        $id = (int) $request->getParam('id', 0);
        // If not exist
        if (!$id) {
            return $this->_helper->redirector('list', $request->getControllerName());
        }

        try {
            $this->getMenuTable()->deleteById($id);
        } catch (Exception $ex) {
            return $this->_helper->redirector('list', $request->getControllerName());
        }
        return $this->_helper->redirector('list', $request->getControllerName());
    }

    public function renderAction() {
        // Get request uri
        $request = $this->getRequest();
        $uri = $request->getRequestUri();

        $menuId = (int) $request->getParam('menuid');

//        $menuItems = $this->getMenuitemTable()->getItemsByMenuId($menuId);
//        var_dump($menuItems);die();
//        if (count($menuItems) > 0) {  
//            foreach ($menuItems as $item) {
//                $label = $item['label'];
//                $image_path = $item['image_path'];
//                $parent_id = $item['parent_id'];
//                if (!empty($item['link'])) {
//                    $uri = $item['link'];
//                } else {
//                    $uri = '/page/open/id/' . $item['page_id'];
//
//                    //update this to form more search-engine-friendly URLs
////                    $page = new CMS_Content_Item_Page($item->page_id);
////                    $uri = '/page/open/title/' . $page->name; 
//                }
//                $itemArray[] = array(
//                    'label' => $label,
//                    'uri' => $uri,
//                    'image_path' => $image_path,
//                    'parent_id' => $parent_id
//                );
//            }
//            $select = $this->getMenuitemTable()->select();
//            $select->where('id = ?', $menuId);
        $where = $this->getMenuTable()->getAdapter()->quoteInto('menu_id = ?', $menuId);
        $row = $this->getMenuitemTable()->fetchAll($where, 'position');
        
        $select = $this->getMenuTable()->select()->order('position');
        
        if ($row->count() > 0) {
            $this->view->menus = $row;

            // check render html for admin or front
            $this->view->menuId = $menuId;
            
            // sort menuitem in view
            $this->view->select = $select;
        }

//        }
    }

    function getMenuitemTable() {
        if (!$this->_menuitemTable) {
            $model = new Admin_Model_Menuitem();
            $this->_menuitemTable = $model;
        }

        return $this->_menuitemTable;
    }

    function getMenuTable() {
        if (!$this->_menuTable) {
            $model = new Admin_Model_Menu();
            $this->_menuTable = $model;
        }

        return $this->_menuTable;
    }

}
