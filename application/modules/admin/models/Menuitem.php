<?php

class Admin_Model_Menuitem extends Zend_Db_Table_Abstract {

    protected $_scheme = 'zf1ms';
    protected $_name = 'menu_items';
    protected $_primary = 'id';
    protected $_referenceMap = array(
        'Menu' => array(
            'columns' => array('menu_id'),
            'refTableClass' => 'Admin_Model_Menu',
            'refColumns' => array('id'),
            'onDelete' => self::CASCADE,
            'onUpdate' => self::RESTRICT
        ),
        'Menuitem' => array(
            'columns' => array('parent_id'),
            'refTableClass' => 'Admin_Model_Menuitem',
            'refColumns' => array('id'),
            'onDelete' => self::CASCADE,
            'onUpdate' => self::RESTRICT
        )
    );
    
    public function __construct() {
        $multiDbResource = Zend_Registry::get('multidb');
        $adapter = $multiDbResource->getDb($this->_scheme);
        parent::__construct($adapter);
    }

    public function get($id = null) {
        $select = $this->select()->from($this->_name);
        if (!$id) {
            // Get all
            $list = $this->getAdapter()->fetchAll($select);
            return $list;
        }
        
        // Get by id
        $id = (int) $id;
        $select->where($this->getAdapter()->quoteInto('id = ?', $id));
        $row = $this->getAdapter()->fetchRow($select);

        if (!$row) {
            throw new Exception("Could not find row $id");
        }

        return $row;
    }

    public function save($data) {
        $id = (int) $data['id'];
        if ($id == 0) { // Insert
            $position = $this->_getLastPosition((int) $data['menu_id']) + 1;
            $data['position'] = $position;
            $this->getAdapter()->insert($this->_name, $data);
        } else { // Update
            if ($data['page_id'] >= 1) {
                $data['link'] = null;
            }
            $where = $this->getAdapter()->quoteInto('id = ?', $id);
            $this->getAdapter()->update($this->_name, $data, $where);
        }
    }

    public function deleteById($id) {
        // Delete
        $where = $this->getAdapter()->quoteInto('id = ?', $id);
        $this->getAdapter()->delete($this->_name, $where);
    }

    public function getItemsByMenuId($menuId) {
        $menuId = (int) $menuId;
        $select = $this->getAdapter()->select()->from($this->_name);
        $select->where("menu_id = ?", $menuId);
        $select->order("position");
        $items = $this->getAdapter()->fetchAll($select);

        return $items;
    }

    private function _getLastPosition($menuId) {
        $select = $this->getAdapter()->select()->from($this->_name);
        $select->where($this->getAdapter()->quoteInto("menu_id = ?", $menuId));
        $select->order('position DESC');
        $row = $this->getAdapter()->fetchRow($select);
        if ($row) {
            return $row['position'];
        } else {
            return 0;
        }
    }

    public function moveUp($itemId) {
        $row = $this->get($itemId);
        if ($row) {
            $position = $row['position'];
            if ($position < 1) {
                // this is already the first item 
                return FALSE;
            } else {
                //find the previous item 
                $select = $this->getAdapter()->select()->from($this->_name);
                $select->order('position DESC');
                $select->where("position < ?", $position);
                $select->where("menu_id = ?", $row['menu_id']);
                $previousItem = $this->getAdapter()->fetchRow($select);
                if ($previousItem) {
                    //switch positions with the previous item 
                    $previousPosition = $previousItem['position'];

                    $previousItem['position'] = $position;
                    $this->save($previousItem);

                    $row['position'] = $previousPosition;
                    $this->save($row);
                }
            }
        } else {
            throw new Zend_Exception("Error loading menu item");
        }
    }

    public function moveDown($itemId) {
        $row = $this->get((int) $itemId);
        if ($row) {
            $position = $row['position'];
            if ($position == $this->_getLastPosition($row['menu_id'])) {
                // this is already the last item 
                return FALSE;
            } else {
                //find the next item 
                $select = $this->getAdapter()->select()->from($this->_name);
                $select->order('position ASC');
                $select->where("position > ?", $position);
                $select->where("menu_id = ?", $row['menu_id']);
                $nextItem = $this->getAdapter()->fetchRow($select);
                if ($nextItem) {
                    //switch positions with the next item 
                    $nextPosition = $nextItem['position'];

                    $nextItem['position'] = $position;
                    $this->save($nextItem);

                    $row['position'] = $nextPosition;
                    $this->save($row);
                }
            }
        } else {
            throw new Zend_Exception("Error loading menu item");
        }
    }

}
