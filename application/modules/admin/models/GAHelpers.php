<?php

//$dir = realpath(APPLICATION_PATH . '/../library/google-api-php-client/src/');
//
//require_once $dir . '/Google_Client.php';
//require_once $dir . '/contrib/Google_AnalyticsService.php';

class Admin_Model_GAHelpers {

    CONST GA_CLIENT_ID = '664761657633-bo0ieb1fdo69v4v9b0rhhk79levq5eun.apps.googleusercontent.com';
    CONST GA_CLIENT_SECRET = '2WEYSPMnck1l1T36Z2eV_5I4';
//    CONST GA_REDIRECT_URL = "http://localhost/ZendFrameworkQuickstart-20100208/public/ga/mbnd";
    CONST GA_DEVELOPER_KEY = 'AIzaSyBYU3DbTSLZN_BKhZx_wrJwfQoV_mPqMNw';
    CONST GA_ANALYTICS_SCOPE = 'https://www.googleapis.com/auth/analytics.readonly';
    CONST GA_APP_NAME = 'ga demo';
    CONST WEB_MARRY_VN = '35405152';
    CONST WEB_MARRYBABY_VN = '42697062'; //profile id
    CONST WEB_ELLE_VN = '57186554';
    CONST WEB_WOMANSHEATHVN_COM = '51956603';
    CONST WEB_BEPGIADINH_COM = '69807888';
    CONST WEB_MUABANNHADAT_COM = '80785431';

    protected $ga;

    public function __construct(Admin_Model_GA $ga) {
        $this->ga = $ga;
    }

    public function getGa() {
        return $this->ga;
    }

    public function init() {
        $this->ga->setClient($this->initClient());
        $this->ga->setService($this->initService($this->ga->getClient()));
    }

    public function initClient() {
        $client = new Google_Client();
        $client->setApplicationName(self::GA_APP_NAME);
        $client->setClientId(self::GA_CLIENT_ID);
        $client->setClientSecret(self::GA_CLIENT_SECRET);
        $client->setDeveloperKey(self::GA_DEVELOPER_KEY);
        $client->setScopes(array(self::GA_ANALYTICS_SCOPE));
        $client->setAccessType('offline'); //refresh token
        $client->setApprovalPrompt('force'); //refresh token
        $client->setUseObjects(true);

        return $client;
    }

    public function initService($client) {
        $service = new Google_AnalyticsService($client);
        return $service;
    }

    function getRealTimeRepost($results) {
        return array(
            'profile' => $this->getProfileInfo($results),
            'total' => $this->getTotalsForAllResults($results),
        );
    }

    function getProfileInfo(&$results) {
        $profileInfo = $results->getProfileInfo();
        return $profileInfo->getProfileName();
    }

    function getTotalsForAllResults(&$results) {
        $totals = $results->getTotalsForAllResults();
        foreach ($totals as $metricName => $metricTotal) {
            return $metricTotal;
        }
    }

    public function getGAOption($pId, $service) {
        $optParams = array(
            'dimensions' => 'ga:medium');
        try {
            $results = $service->data_realtime->get(
                    'ga:' . $pId, 'ga:activeVisitors', $optParams
            );
            // Success.
        } catch (apiServiceException $e) {
            // Handle API service exceptions.
            $error = $e->getMessage();
        }
        return $results;
    }

    public function getServiceMbnd() {
//        $data = file_get_contents('http://ws-mbnd-data.muabannhadat.com.vn/Services/ObjectCounting.asmx/GetTotalListing');
//        $data = json_decode($data);
//        print_r($data); die();
        $data = simplexml_load_file('http://ws-mbnd-data.muabannhadat.com.vn/Services/ObjectCounting.asmx/GetTotalListing');
        $data = json_decode($data, true);
        return $data;
    }

}
