<?php

class Admin_Model_GA {

    protected $client;
    protected $service;
    protected $result;

    public function setClient($client) {
        $this->client = $client;
        return $this;
    }

    public function getClient() {
        return $this->client;
    }

    public function setService($service) {
        $this->service = $service;
        return $this;
    }

    public function getService() {
        return $this->service;
    }

    public function setResult($result) {
        $this->result = $result;
        return $this;
    }

    public function getResult() {
        return $this->result;
    }

}
