<?php

class Admin_Model_Menu extends Zend_Db_Table_Abstract {

    protected $_scheme = 'zf1ms';
    protected $_name = 'menus';
    protected $_primary = 'id';
    protected $_dependentTables = array('Admin_Model_MenuItem');
    protected $_referenceMap = array(
        'Menu' => array(
            'columns' => array('parent_id'),
            'refTableClass' => 'Admin_Model_Menu',
            'refColumns' => array('id'),
            'onDelete' => self::CASCADE,
            'onUpdate' => self::RESTRICT
        )
    );

    public function __construct() {
        $multiDbResource = Zend_Registry::get('multidb');
        $adapter = $multiDbResource->getDb($this->_scheme);
        parent::__construct($adapter);
    }

    public function get($id = null) {
        $select = $this->select()->from($this->_name);
        if (!$id) {
            // Get all
            $list = $this->getAdapter()->fetchAll($select);
            return $list;
        }

        // Get by id
        $id = (int) $id;
        $select->where($this->getAdapter()->quoteInto('id = ?', $id));
        $row = $this->getAdapter()->fetchRow($select);

        if (!$row) {
            throw new Exception("Could not find row $id");
        }
        
        return $row;
    }

    public function save($data) {
        $id = (int) $data['id'];
        if ($id == 0) {
            // Insert
            $this->getAdapter()->insert($this->_name, $data);
            return;
        }

        // Update
        $where = $this->getAdapter()->quoteInto('id = ?', $id);
        $this->getAdapter()->update($this->_name, $data, $where);
    }

    public function deleteById($id) {
        // Delete
        $where = $this->getAdapter()->quoteInto('id = ?', $id);
        $this->getAdapter()->delete($this->_name, $where);
    }

}
