<?php

class Admin_Model_User extends Zend_Db_Table_Abstract {

    protected $_scheme = 'zf1ms';
    protected $_name = 'users';
    protected $_primary = 'id';

    public function __construct() {
        $multiDbResource = Zend_Registry::get('multidb');
        $adapter = $multiDbResource->getDb($this->_scheme);
        parent::__construct($adapter);
    }

    public function get($id = null) {
        $select = $this->getAdapter()->select()->from($this->_name);
        if (!$id) {
            // Get all
            $list = $this->getAdapter()->fetchAll($select);
            return $list;
        }

        // Get by id
        $select->where($this->getAdapter()->quoteInto('id = ?', $id));
        $row = $this->getAdapter()->fetchRow($select);

        return $row;
    }

    public function save($data) {
        $id = (int) $data['id'];
        if ($id == 0) {
            // Insert
            $data['password'] = md5($data['password']);
            $this->getAdapter()->insert($this->_name, $data);
        } else {
            // Update
            $where = $this->getAdapter()->quoteInto('id = ?', $id);
            $this->getAdapter()->update($this->_name, $data, $where);
        }
    }

    public function updatePassword($id, $password) {
        // fetch the user's row 
        $row = $this->get($id);

        if ($row) {
            //update the password 
            $row['password'] = md5($password);
            $this->save($row);
        } else {
            throw new Zend_Exception("Password update failed.  User not found!");
        }
    }

    public function deleteById($id) {
        // Delete
        $where = $this->getAdapter()->quoteInto('id = ?', $id);
        $this->getAdapter()->delete($this->_name, $where);
    }

}
