<?php

class Zend_View_Helper_LoadSkin extends Zend_View_Helper_Abstract
{
    public function loadSkin($skin)
    {
        // load the skin config file
        $skinData = new Zend_Config_Xml('./skins/' . $skin . '/skin.xml');

        if (is_object($skinData->stylesheets)) {
            if (is_string($skinData->stylesheets->stylesheet)) {
                $this->view->headLink()->appendStylesheet('/skins/' . $skin . '/css/' . $skinData->stylesheets->stylesheet);
            } else {
                $stylesheets = $skinData->stylesheets->stylesheet->toArray();

                if (is_array($stylesheets)) {
                    foreach ($stylesheets as $stylesheet) {
                        $this->view->headLink()->appendStylesheet('/skins/' . $skin .
                            '/css/' . $stylesheet);
                    }
                }
            }
        }

        if (is_object($skinData->javascripts)) {
            if (is_string($skinData->javascripts->js)) {
                $this->view->inlineScript()->appendFile('/skins/' . $skin . '/js/' . $skinData->javascripts->js);
            } else {
                $jss = $skinData->javascripts->js->toArray();
                // append each stylesheet
                if (is_array($jss)) {
                    foreach ($jss as $val) {
                        $this->view->inlineScript()->appendFile('/skins/' . $skin . '/js/' . $val);
                    }
                }
            }
        }
    }
}
