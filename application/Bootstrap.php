<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    public function _initModuleResourceAutoloader() {
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->setFallbackAutoloader(false);
    }

    public function _initDb() {
        $this->bootstrap('multidb');
        $multidb = $this->getPluginResource('multidb');
        Zend_Registry::set('multidb', $multidb);
    }

    protected function _initViewSettings() {
//        $this->_logger->info('Bootstrap ' . __METHOD__);

        $this->bootstrap('view');

        $this->_view = $this->getResource('view');

        // add global helpers
        $this->_view->addHelperPath(APPLICATION_PATH . '/views/helpers', 'Zend_View_Helper');

        // set encoding and doctype
        $this->_view->setEncoding('UTF-8');
        $this->_view->doctype('XHTML1_STRICT');

        // set the content type and language
        $this->_view->headMeta()->appendName('viewport', 'width=device-width, initial-scale=1, maximum-scale=1');
        $this->_view->headMeta()->appendHttpEquiv('Content-Type', 'text/html; charset=UTF-8');
        $this->_view->headMeta()->appendHttpEquiv('Content-Language', 'en-US');

        // set css links and a special import for the accessibility styles
//        $this->_view->headStyle()->setStyle('@import "/css/access.css";');
//        $this->_view->headLink()->appendStylesheet('/css/reset.css');
//        $this->_view->headLink()->appendStylesheet('/css/main.css');
//        $this->_view->headLink()->appendStylesheet('/css/form.css');
        // setting the site in the title
        $this->_view->headTitle('zf1_ga');

        // setting a separator string for segments:
        $this->_view->headTitle()->setSeparator(' - ');

        // skin
        $this->_view->skin = 'blues';

        // skin admin
        $this->_view->skinAdmin = 'admin';
    }

    protected function _initMenus() {
        $view = $this->getResource('view');
        $view->mainMenuId = 4;
        $view->adminMenuId = 7;
    }

}
