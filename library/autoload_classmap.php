<?php

return array(
    'Google_Client' => dirname(__FILE__) . '/google-api-php-client/src/Google_Client.php',
    'Google_AnalyticsService' => dirname(__FILE__) . '/google-api-php-client/src/contrib/Google_AnalyticsService.php',
);
