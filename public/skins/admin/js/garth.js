/**
 * Created by me on 4/1/14.
 */

//$.noConflict();
$(document).ready(function () {
    var pathname = window.location.pathname;

    if (pathname == "/admin/ga/mbnd") {
        setInterval(updateMbnd, 3000);
    } else if (pathname == "/admin/ga/digital") {
        setInterval(updateDigital, 3000);
    }

    function updateMbnd() {
        $.ajax({
            url: "/admin/ga/get-info-gart/name/80785431",   // MBND
            type: "GET",
            dataType: "json"
        }).done(function (data) {
            if (typeof data === 'string') {
                window.location.replace("/admin/ga");
            }
//            $("span#mbnd-profile").text(data.profile);
            $("span#mbnd-total").text(data.total);
        });

        $.ajax({
            url: "/admin/ga/get-service-mbnd",
            type: "GET",
            dataType: "json"
        }).done(function (data) {
//            alert(data.toSource());
            $("span#mbnd-yesterday").text(data.yesterday);
            $("span#mbnd-lastmonth").text(data.lastmonth);
        });
    }

    function updateDigital() {
        $.ajax({
            url: "/admin/ga/get-info-gart/name/35405152",  //mary
            type: "GET",
            dataType: "json"
        }).done(function (data) {
            if (typeof data === 'string') {
                window.location.replace("/admin/ga");
            }
//            alert(data.toSource());
//            $("span#marry-profile").text(data.profile);
            $("span#marry-total").text(data.total);
        })

        $.ajax({
            url: "/admin/ga/get-info-gart/name/42697062",  //marybaby
            type: "GET",
            dataType: "json"
        }).done(function (data) {
//            alert(data.toSource());
//            $("span#marrybaby-profile").text(data.profile);
            $("span#marrybaby-total").text(data.total);
        })

        $.ajax({
            url: "/admin/ga/get-info-gart/name/57186554",  //ELLE
            type: "GET",
            dataType: "json"
        }).done(function (data) {
//            alert(data.toSource());
//            $("span#elle-profile").text(data.profile);
            $("span#elle-total").text(data.total);
        })

        $.ajax({
            url: "/admin/ga/get-info-gart/name/51956603",  //WOMANSHEATHVN
            type: "GET",
            dataType: "json"
        }).done(function (data) {
//            alert(data.toSource());
//            $("span#womansheathvn-profile").text(data.profile);
            $("span#womansheathvn-total").text(data.total);
        })

        $.ajax({
            url: "/admin/ga/get-info-gart/name/69807888",  //BEPGIADINH
            type: "GET",
            dataType: "json"
        }).done(function (data) {
//            alert(data.toSource());
//            $("span#bgd-profile").text(data.profile);
            $("span#bgd-total").text(data.total);
        })
    }
})